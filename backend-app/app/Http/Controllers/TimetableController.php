<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Clinic, Doctor, Timeslot};
use Carbon\Carbon;

class TimetableController extends Controller {
  public function timetable(Request $request) {
    $dateStart = Carbon::parse($request->dateStart)->format('Y-m-d H:i:s');
    $dateEnd = Carbon::parse($request->dateEnd)->endOfDay()->format('Y-m-d H:i:s');
    
    $filterClinicId = $request->filters['clinicId'] ?? null;
    $filterDoctorId = $request->filters['doctorId'] ?? null;
    $filterDateEnd = $request->filters['dateEnd'] ?? null;
    $filterDateEnd = $filterDateEnd 
      ? Carbon::parse($filterDateEnd)->endOfDay()->format('Y-m-d H:i:s')
      : null;
    

    $hours = collect(range(8, 19)) //clinics working hours
      ->transform(function($timeStart) {
        $timeEnd = $timeStart + 1;
        return $timeStart . ':00 - ' . $timeEnd . ':00';
      });

    $doctors = Doctor::all()
      ->sortBy('name')
      ->transform(function($doc) {
        return ['value' => $doc->id, 'text' => $doc->name];
      })
      ->values();

    $clinics = Clinic::all()
      ->sortBy('name')
      ->transform(function($clinic) {
        return ['value' => $clinic->id, 'text' => $clinic->name];
      })
      ->values();


    $timeslots = Timeslot::where('datetime_start', '>=', $dateStart)
      ->where('datetime_start', '<=', $dateEnd)
      ->when($filterClinicId, function($query) use($filterClinicId) {
        $query->where('clinic_id', $filterClinicId);
      })
      ->when($filterDoctorId, function($query) use($filterDoctorId) {
        $query->where('doctor_id', $filterDoctorId);
      })
      ->when($filterDateEnd, function($query) use($filterDateEnd) {
        $query->where('datetime_start', '<=', $filterDateEnd);
      })
      ->orderBy('datetime_start')
      ->with('doctor', 'clinic')
      ->get()
      ->transform(function($timeslot) {
        $datetimeStart = $timeslot->datetime_start;
        $date = $datetimeStart->format('d.m.Y');
        $timeStart = $datetimeStart->format('G:i');
        $timeEnd = $timeslot->datetime_end->format('G:i');

        return [
          'id' => $timeslot->id,
          'date' => $date,
          'clinic' => $timeslot->clinic->name,
          'doctor' => $timeslot->doctor->name,
          'time' => $timeStart . ' - ' . $timeEnd,
          'busy' => $timeslot->busy
        ];
      })
      ->groupBy(['time', 'date']);


    return [
      'hours' => $hours,
      'clinics' => $clinics,
      'doctors' => $doctors,
      'timeslots' => $timeslots
    ];
  }
}