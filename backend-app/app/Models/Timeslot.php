<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model {
  public $timestamps = false;
  protected $guarded = ['id'];
  protected $dates = ['datetime_start', 'datetime_end'];
  
  public function doctor() {
    return $this->belongsTo(Doctor::class);
  }

  public function clinic() {
    return $this->belongsTo(Clinic::class);
  }
}
