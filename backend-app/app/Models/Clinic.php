<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model {
  public $timestamps = false;
  protected $guarded = ['id'];

  public function timeslots() {
    return $this->hasMany(Timeslot::class);
  }
}
