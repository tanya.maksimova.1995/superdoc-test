<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeslotsTable extends Migration {
  public function up() {
    Schema::create('timeslots', function (Blueprint $table) {
      $table->id();
      $table->bigInteger('clinic_id')->unsigned();
      $table->bigInteger('doctor_id')->unsigned();
      $table->dateTime('datetime_start');
      $table->dateTime('datetime_end');
      $table->boolean('busy')->default(false);

      $table->foreign('clinic_id')->references('id')->on('clinics')->onDelete('cascade');
      $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
    });
  }


  public function down() {
    Schema::dropIfExists('timeslots');
  }
}
