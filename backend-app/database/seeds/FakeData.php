<?php

use Illuminate\Database\Seeder;
use App\Models\{Clinic, Doctor, Timeslot};
use Faker\Generator as Faker;

class FakeData extends Seeder {
  public function run() {
    $clinicIds = factory(Clinic::class, 10)->create()->pluck('id');
    $doctorIds = factory(Doctor::class, 20)->create()->pluck('id');

    $timeslots = factory(Timeslot::class, 100)->make()
      ->each(function($timeslot) use($doctorIds, $clinicIds) {
        $timeslot->doctor_id = $doctorIds->random();
        $timeslot->clinic_id = $clinicIds->random();
        $timeslot->save();
      });
  }
}
