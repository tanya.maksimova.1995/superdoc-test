<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Timeslot;
use Faker\Generator as Faker;

$factory->define(Timeslot::class, function (Faker $faker) {
  $hour = rand(8, 19); //clinics working hours
  $dateTimeStart = $faker->dateTimeBetween($startDate = '-1 month', $endDate = '+1 month')
    ->setTime($hour, 0);

  $dateTimeEnd = clone $dateTimeStart;
  $dateTimeEnd->add(new DateInterval('PT1H'));

  return [
    'datetime_start' => $dateTimeStart,
    'datetime_end' => $dateTimeEnd,
    'busy' => $faker->boolean
  ];
});
