export interface Filters {
  clinicId?: number | null,
  doctorId?: number | null,
  dateStart?: string | null,
  dateEnd?: string | null,
}

export interface FilterParams {
  value: number | null,
  text: string
}

export interface Timeslots {
  [key: string]: {
    [key: string]: Timeslot[]
  }
}

export interface Timeslot {
  busy: number
  clinic: string
  date: string
  doctor: string
  id: number
  time: string
}