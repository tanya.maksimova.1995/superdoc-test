import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Timetable from '../views/Timetable.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Timetable',
    component: Timetable
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
